import {RouterModule,Routes} from '@angular/router';
import{
    RegistrarConvenioComponent,
    ConsultarConvenioComponent,
    BlankComponent,
    GantComponent,
    LoginComponent,
    AdminComponent
} from './components/index.paginas';
const app_routes: Routes=[
    {path:'login', component:LoginComponent},
    {path:'admin', component:AdminComponent,
        children: [
            {path:'', component: BlankComponent},
            {path:'registrar', component: RegistrarConvenioComponent},
            {path:'consultar', component: ConsultarConvenioComponent},
            {path:'gant', component: GantComponent},
            {path:'**',pathMatch:'full',redirectTo:'blank'}
        ]
    },
    {path:'**',pathMatch:'full',redirectTo:'login'}      
];

export const app_routing=RouterModule.forRoot(app_routes);