import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public Server = 'http://172.16.179.42:9095/';
    public ApiUrl = 'scoopera/api/v1/';
    public ServerWithApiUrl = this.Server + this.ApiUrl;
}