import { Component, OnInit, ElementRef, ViewChild, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ConveniosService } from "../../../../services/convenios.service";
import { Convenio } from "../../../../model/convenioModel"
import { ToasterService } from "../../../../services/toaster.service";

declare var jQuery: any;
declare var $: any;
@Component({
  selector: 'app-registrar-convenio',
  templateUrl: './registrar-convenio.component.html',
  styleUrls: ['./registrar-convenio.component.css']
})
export class RegistrarConvenioComponent implements OnInit {

  pdfSrc: string;
  verPdf: boolean = false;
  datePickerConfig = {
    locale: "es",
    format: "DD/MM/YYYY"
  }
  /* a:ToastsManager;
  b:ViewContainerRef;
  mensajeModal= new ModalMessageComponent(this.a,this.b); */
  convenioForm: FormGroup;
  nombrePdf = "Seleccione un archivo";
  loading: boolean = false;

  @ViewChild('fileInput') fileInput: ElementRef;
  constructor(private fb: FormBuilder, private _cs: ConveniosService, public _ts: ToasterService) {
    this.createForm();
  }

  createForm() {
    this.convenioForm = this.fb.group({
      codConvenio: '',
      desConvenio: '',
      tituloConvenio: '',
      txtExpediente: '',
      txtResRectoral: '',
      txtCodArchivo: '',
      archivoConvenio: '',//this.pdfArchivo
      idInstitucionOrigen: '',
      idInstitucionDestino: '',
      idTipoConvenio: '',
      idModalidad: '',
      estado: null,
      fechaIngreso: '',
      fechaSuscripcion: '',
      fechaCese: '',
      fechaCreacion: null,
      usuarioCreacion: null,
      usuarioModificacion: null,
      poblacionBeneficiada: '',
      monto: 0.00,
      rrhhInvolucrados: '',
      plantrabajo: ''
    });
  }
  ngOnInit() {
    var pdf_link: String;

    //botonesVerticalHorizontal      
    var mediaquery = window.matchMedia("(min-width: 576px)");
    console.log(mediaquery);
    function handleOrientationChange(mediaquery) {
      if (mediaquery.matches) {
        var d = document.getElementById("botones");
        $(d).removeClass('btn-group-vertical').addClass('btn-group');
      } else {
        var d = document.getElementById("botones");
        $(d).removeClass('btn-group').addClass('btn-group-vertical');
      }
    }
    handleOrientationChange(mediaquery);
    mediaquery.addListener(handleOrientationChange);

    (function (a) { a.createModal = function (b) { var defaults = { title: "", message: "Your Message Goes Here!", closeButton: true, scrollable: false }; var b = a.extend({}, defaults, b); var c = (b.scrollable === true) ? 'style="max-height: 500px;overflow-y:auto;"' : ""; var html = '<div class="modal fade" id="myModal">'; html += '<div class="modal-dialog">'; html += '<div class="modal-content" style="height:80vh">'; html += '<div class="modal-header" style="display:initial">'; html += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'; if (b.title.length > 0) { html += '<h4 class="modal-title">' + b.title + "</h4>" } html += "</div>"; html += '<div class="modal-body" ' + c + ">"; html += b.message; html += "</div>"; html += '<div class="modal-footer">'; if (b.closeButton === true) { html += '<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>' } html += "</div>"; html += "</div>"; html += "</div>"; html += "</div>"; a("body").prepend(html); a("#myModal").modal().on("hidden.bs.modal", function () { a(this).remove() }) } })(jQuery);

    /*
    * Here is how you use it
    */
    $('.view-pdf').on('click', function () {
      pdf_link = $(this).attr('href');
      console.log(pdf_link)
      var iframe = '<div  width="100%" height="100%" class="iframe-container"><iframe src="' + pdf_link + '"></iframe></div>'
      $.createModal({
        title: 'TITULO ',
        message: iframe,
        closeButton: true,
        scrollable: false
      });
      return false;
    });
    $('.dp-picker-input').addClass('form-control');
    $('.dp-picker-input').css({ "height": "initial", "width": "100%", "font-size": "initial" });
    var widthDate = $('.input-group').width() - 40.859;
    $('dp-date-picker').width(widthDate);
  }

  onFileChange(event) {

    let $pdf: any = document.querySelector('#archivoConvenio');
    let reader = new FileReader();
    let reader1 = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      //console.log("local",reader.readAsDataURL(file));
      reader.onload = () => {
        this.convenioForm.get('archivoConvenio').setValue({
          nombre: file.name,
          base64string: reader.result.split(',')[1]
        })
        this.nombrePdf = this.convenioForm.get('archivoConvenio').value.nombre;
      };
      reader1.onload = (e: any) => {
        this.pdfSrc = e.target.result;
      }
      reader1.readAsArrayBuffer($pdf.files[0]);
      this.verPdf = true;
    }
  }
  onSubmit() {
    const formModel: Convenio = this.convenioForm.value;
    console.log(formModel)
    this.loading = true;
    this._cs
      .add(formModel)
      .subscribe(
        data => {
          let respuesta = +data;
          if (respuesta == 1) {
            this.convenioForm.reset(
              {
                codConvenio: '',
                desConvenio: '',
                tituloConvenio: '',
                txtExpediente: '',
                txtResRectoral: '',
                txtCodArchivo: '',
                archivoConvenio: ['', Validators.required],//this.pdfArchivo
                idInstitucionOrigen: '',
                idInstitucionDestino: '',
                idTipoConvenio: '',
                idModalidad: '',
                estado: null,
                fechaIngreso: '',
                fechaSuscripcion: '',
                fechaCese: '',
                fechaCreacion: null,
                usuarioCreacion: null,
                usuarioModificacion: null,
                poblacionBeneficiada: '',
                monto: 0.00,
                rrhhInvolucrados: '',
                plantrabajo: ''
              }
            );
            this._ts.showSuccess("Convenio registrado.", "Exito");
          }
          else if (respuesta == 0) {
            this._ts.showWarning("No se pudo registrar el convenio!", "Alerta");
          }
          this.nombrePdf = "Seleccione un archivo";
          this.loading = false;
          this.verPdf = false;
        }, error => {
          this.nombrePdf = "Seleccione un archivo";
          this.loading = false;
          this.verPdf = false;
          console.log("error")
          console.log(error);
          this._ts.showError("Ocurrio un error!", "Ops");
        });
  }
  clearForm() {
    this.convenioForm.get('pdfArchivo').setValue(null);
    this.convenioForm.get('codConvenio').setValue(null);
    this.fileInput.nativeElement.value = '';
  }
}
