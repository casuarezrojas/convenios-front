import { Component, OnInit } from '@angular/core';
import { InformacionService } from "../../services/informacion.service"
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  constructor (public _is:InformacionService){
  }
  ngOnInit() {
    // Toggle the side navigation
    $("#sidenavToggler").click(function (e) {
      e.preventDefault();
      console.log("prro")
      var $body = $("body");
      var $userCard = $('.userCard');
      if ($body.hasClass("sidenav-toggled")) {
        $body.removeClass("sidenav-toggled");
        $userCard.removeClass("ocultar");
      }
      else {
        $body.addClass("sidenav-toggled");
        $userCard.addClass("ocultar");
      }
      $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
      $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    });
    // Force the toggled class to be removed when a collapsible nav link is clicked
    $(".navbar-sidenav .nav-link-collapse").click(function (e) {
      e.preventDefault();
      var $body = $("body");
      var $userCard = $('.userCard');
      if ($body.hasClass("sidenav-toggled")) {
        $userCard.removeClass("ocultar");
      }
      $("body").removeClass("sidenav-toggled");
    });
  }
}
