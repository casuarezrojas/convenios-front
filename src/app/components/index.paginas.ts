export { RegistrarConvenioComponent } from "./content-wrapper/contenedor-body/registrar-convenio/registrar-convenio.component";
export { ConsultarConvenioComponent } from "./content-wrapper/contenedor-body/consultar-convenio/consultar-convenio.component";
export { BlankComponent } from "./content-wrapper/contenedor-body/blank/blank.component";
export { GantComponent } from "./content-wrapper/contenedor-body/gant/gant.component";
export { LoginComponent} from "./login/login.component";
export { AdminComponent } from "./admin/admin.component";