import { Component,ViewContainerRef } from '@angular/core';
import { InformacionService } from './services/informacion.service'
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(  public _is:InformacionService,public toastr: ToastsManager, vcr:ViewContainerRef ){
    toastr.setRootViewContainerRef(vcr);
  }
}
