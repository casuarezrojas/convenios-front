import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import { Configuration } from '../app.configuration';
import { HttpHeaders } from '@angular/common/http';
import { Convenio } from '../model/convenioModel'

const httpOptions = {
  headers: new HttpHeaders({
    responseType: 'text',
    'Content-Type': 'application/json'
  })
};
@Injectable()
export class ConveniosService {

  private actionUrl: string;


  constructor(private http: HttpClient, private _configuration: Configuration) {
    this.actionUrl = _configuration.ServerWithApiUrl + 'convenios/';
  }

  public getAll<T>(): Observable<T> {
    return this.http.get<T>(this.actionUrl);
  }

  /* public getSingle<T>(id: number): Observable<Convenio> {
    return this.http.get<T>(this.actionUrl + id);
  } */

  public add(convenio: Convenio): Observable<string> { //<Convenio>

    //const body = JSON.stringify(convenio);
    return this.http
        .post(this.actionUrl, convenio, {responseType: 'text'})
        .catch(this.handleError);
  }

  /* private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  } */
  private handleError (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  } 
  /* 
    addPost(convenio: any): Observable {
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers });
  
      return this.http.post(this.postUrl, { post }, options)
        .map(this.parseData)
        .catch(this.handleError);
    }
  
  
    // Updated parse data method handles arrays and objects
    private parseData(res: Response) {
      let body = res.json();
  
      if (body instanceof Array) {
        return body || [];
      }
  
      else return body.post || {};
    }
   */
  public update<T>(id: number, itemToUpdate: any): Observable<T> {
    return this.http
      .put<T>(this.actionUrl + id, JSON.stringify(itemToUpdate));
  }

  public delete<T>(id: number): Observable<T> {
    return this.http.delete<T>(this.actionUrl + id);
  }
}
/* @Injectable()
export class CustomInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    }

    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
    console.log(JSON.stringify(req.headers));
    return next.handle(req);
  }
} */
