import { Injectable} from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Injectable()
export class ToasterService {

  constructor(public toastr: ToastsManager) {
  }
  showSuccess(mSucces:string,succes:string) {
    this.toastr.success(mSucces, succes);
  }

  showError(mError:string,error:string) {
    this.toastr.error(mError, error);
  }

  showWarning(mWarning:string, warning:string) {
    this.toastr.warning(mWarning, warning);
  }

  showInfo() {
    this.toastr.info('Just some information for you.');
  }

  showCustom() {
    this.toastr.custom('<span style="color: red">Message in red.</span>', null, { enableHTML: true });
  }
  mensaje(){
    console.log("nomames")
  }
}
