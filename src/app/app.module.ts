import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DpDatePickerModule} from 'ng2-date-picker';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ng2-bootstrap'
import { TabsModule } from 'ng2-bootstrap';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { PdfViewerModule } from 'ng2-pdf-viewer';



//rutas
import {app_routing} from "./app.routes";
import {Configuration} from "./app.configuration";

//servicios
import {InformacionService} from "./services/informacion.service";
import {ConveniosService} from "./services/convenios.service";
import {ToasterService} from "./services/toaster.service"

//componentes
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { ContentWrapperComponent } from './components/content-wrapper/content-wrapper.component';
import { FooterComponent } from './components/content-wrapper/footer/footer.component';
import { ContenedorBodyComponent } from './components/content-wrapper/contenedor-body/contenedor-body.component';
import { ModalLogoutComponent } from './components/content-wrapper/modal-logout/modal-logout.component';
import { BreadCrumbComponent } from './components/content-wrapper/contenedor-body/bread-crumb/bread-crumb.component';
import { RegistrarConvenioComponent } from './components/content-wrapper/contenedor-body/registrar-convenio/registrar-convenio.component';
import { ConsultarConvenioComponent } from './components/content-wrapper/contenedor-body/consultar-convenio/consultar-convenio.component';
import { BlankComponent } from './components/content-wrapper/contenedor-body/blank/blank.component';
import { ModalqrComponent } from './components/content-wrapper/contenedor-body/consultar-convenio/modalqr/modalqr.component';
import { ModalDetalleConvenioComponent } from './components/content-wrapper/contenedor-body/consultar-convenio/modal-detalle-convenio/modal-detalle-convenio.component';
import { GantComponent } from './components/content-wrapper/contenedor-body/gant/gant.component';
import { LoginComponent } from './components/login/login.component';
import { AdminComponent } from './components/admin/admin.component';
import { ModalMessageComponent } from './components/content-wrapper/modal-message/modal-message.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ContentWrapperComponent,
    FooterComponent,
    ContenedorBodyComponent,
    ModalLogoutComponent,
    BreadCrumbComponent,
    RegistrarConvenioComponent,
    ConsultarConvenioComponent,
    BlankComponent,
    ModalqrComponent,
    ModalDetalleConvenioComponent,
    GantComponent,
    LoginComponent,
    AdminComponent,
    ModalMessageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    app_routing,
    FormsModule,
    ReactiveFormsModule,
    DpDatePickerModule,
    Ng2TableModule,
    PaginationModule.forRoot(),
    TabsModule,
    HttpClientModule,
    ToastModule.forRoot(),
    BrowserAnimationsModule, 
    PdfViewerModule
  ],
  providers: [
    InformacionService,
    ConveniosService,
    ToasterService,
    Configuration
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
